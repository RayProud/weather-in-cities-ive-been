var app = angular.module('Weather', ['ngRoute']);

app.config(function($routeProvider) {
  $routeProvider
    .when('/', {
      controller: 'WeatherHomeController',
      templateUrl: '../views/home.html'
    })
    .when('/city/:id/:index', {
      controller: 'WeatherCityController',
      templateUrl: '../views/currentCity.html'
    })
    .otherwise({
      redirectTo: '/'
    });
});
