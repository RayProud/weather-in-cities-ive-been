app.factory('weather', ['$http', function($http) {
  return $http
    .get('http://api.openweathermap.org/data/2.5/group?id=1496747,519690,554234,1489425,1502026&units=metric')
    .then(
        function(data) {
          return data;
        },
        function(err) {
          return err;
        }
      );
}]);