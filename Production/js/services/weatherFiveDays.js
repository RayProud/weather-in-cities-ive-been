app.factory('weatherFiveDays', ['$http', function($http) {
  return function(cityID) {
    return $http
      .get('http://api.openweathermap.org/data/2.5/forecast/daily?id=' + cityID + '&units=metric&cnt=5')
      .then(
          function(data) {
            return data.data;
          },
          function(err) {
            return err;
          }
        );
  };
}]);