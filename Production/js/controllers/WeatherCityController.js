app.controller('WeatherCityController', ['$scope', '$routeParams', 'weatherFiveDays', function($scope, $routeParams, weatherFiveDays) {

  $scope.cityImages = [
    'http://img-fotki.yandex.ru/get/4908/raskalov.52/0_49592_216ad7b2_orig',
    'http://www.holidays-rus.ru/cache/gallery/13_s_s01_00SPB008.jpg',
    'http://swena.ru/wp-content/uploads/2013/04/kafedralnyi-sobor.jpg',
    'http://voinskayachast.ru/wp-content/uploads/2015/01/%D1%82%D0%BE%D0%BC%D1%81%D0%BA.jpg',
    'http://www.krasfair.ru/upload/tmp/krasnoyarsk.jpg'
  ];

  $scope.cityIndex = $routeParams.index;
  $scope.cityID = $routeParams.id;
  $scope.error = false;

  var makingSvg = function(list) {
    var svgHeight = 300;
    var svgWidth = 600;
    var xPart = svgWidth/list.length;
    var temp5DayData = list.map(function (item, i, arr) {
      return Math.round(arr[i].temp.day);
    });
    var yMin = Math.min.apply(null, temp5DayData);
    var yMax = Math.max.apply(null, temp5DayData);
    var yHeight = yMax - yMin;
    var yPart = svgHeight/yHeight;

    var svg = d3.select('#svg-wrap')
                .append('svg')
                .attr('height', svgHeight)
                .attr('width', svgWidth)
                .attr('id', 'svg');

      dots = svg.selectAll('circle')
                .data(list)
                .enter()
                .append('circle');

      dots.attr('r', 5)
          .attr('cx', function(d, i) {
            return Math.abs(i * xPart);
          })
          .attr('cy', function(d) {
            return svgHeight - ((d.temp.day/yMin) * yPart);
          })
  };


  weatherFiveDays($scope.cityID)
    .then(
        function(data) {
          if (data.cod == "404") {
            $scope.error = true;
            return false;
          }

          $scope.currentCityWeather = data;

          makingSvg(data.list);
        }
      );

}]);