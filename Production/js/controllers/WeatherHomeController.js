app.controller('WeatherHomeController', ['$scope', 'weather', function($scope, weather) {

  $scope.cityImages = [
  'http://img-fotki.yandex.ru/get/4908/raskalov.52/0_49592_216ad7b2_orig',
  'http://www.holidays-rus.ru/cache/gallery/13_s_s01_00SPB008.jpg',
  'http://swena.ru/wp-content/uploads/2013/04/kafedralnyi-sobor.jpg',
  'http://voinskayachast.ru/wp-content/uploads/2015/01/%D1%82%D0%BE%D0%BC%D1%81%D0%BA.jpg',
  'http://www.krasfair.ru/upload/tmp/krasnoyarsk.jpg'
  ];

  weather
    .then(
      function(data) {
        if (data.status != 200) {
          $scope.error = true;
          return false;
        }

        $scope.weatherList = data.data.list;
      }
    );

}]);